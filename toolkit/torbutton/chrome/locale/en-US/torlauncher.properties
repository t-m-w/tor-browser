# Copyright (c) 2022, The Tor Project, Inc.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

torlauncher.error_title=Tor Launcher

torlauncher.tor_exited_during_startup=Tor exited during startup. This might be due to an error in your torrc file, a bug in Tor or another program on your system, or faulty hardware. Until you fix the underlying problem and restart Tor, Tor Browser will not start.
torlauncher.tor_exited=Tor unexpectedly exited. This might be due to a bug in Tor itself, another program on your system, or faulty hardware. Until you restart Tor, Tor Browser will not be able to reach any websites. If the problem persists, please send a copy of your Tor Log to the support team.
torlauncher.tor_exited2=Restarting Tor will not close your browser tabs.
torlauncher.restart_tor=Restart Tor
torlauncher.tor_controlconn_failed=Could not connect to Tor control port.
torlauncher.tor_bootstrap_failed=Tor failed to establish a Tor network connection.
torlauncher.tor_bootstrap_failed_details=%1$S failed (%2$S).

torlauncher.unable_to_start_tor=Unable to start Tor.\n\n%S
torlauncher.tor_missing=The Tor executable is missing.
torlauncher.torrc_missing=The torrc file is missing and could not be created.
torlauncher.datadir_missing=The Tor data directory does not exist and could not be created.
torlauncher.onionauthdir_missing=The Tor onion authentication directory does not exist and could not be created.
torlauncher.password_hash_missing=Failed to get hashed password.

torlauncher.bootstrapStatus.starting=Starting
torlauncher.bootstrapStatus.conn_pt=Connecting to bridge
torlauncher.bootstrapStatus.conn_done_pt=Connected to bridge
torlauncher.bootstrapStatus.conn_proxy=Connecting to proxy
torlauncher.bootstrapStatus.conn_done_proxy=Connected to proxy
torlauncher.bootstrapStatus.conn=Connecting to a Tor relay
torlauncher.bootstrapStatus.conn_done=Connected to a Tor relay
torlauncher.bootstrapStatus.handshake=Negotiating with a Tor relay
torlauncher.bootstrapStatus.handshake_done=Finished negotiating with a Tor relay
torlauncher.bootstrapStatus.onehop_create=Establishing an encrypted directory connection
torlauncher.bootstrapStatus.requesting_status=Retrieving network status
torlauncher.bootstrapStatus.loading_status=Loading network status
torlauncher.bootstrapStatus.loading_keys=Loading authority certificates
torlauncher.bootstrapStatus.requesting_descriptors=Requesting relay information
torlauncher.bootstrapStatus.loading_descriptors=Loading relay information
torlauncher.bootstrapStatus.enough_dirinfo=Finished loading relay information
torlauncher.bootstrapStatus.ap_conn_pt=Building circuits: Connecting to bridge
torlauncher.bootstrapStatus.ap_conn_done_pt=Building circuits: Connected to bridge
torlauncher.bootstrapStatus.ap_conn_proxy=Building circuits: Connecting to proxy
torlauncher.bootstrapStatus.ap_conn_done_proxy=Building circuits: Connected to proxy
torlauncher.bootstrapStatus.ap_conn=Building circuits: Connecting to a Tor relay
torlauncher.bootstrapStatus.ap_conn_done=Building circuits: Connected to a Tor relay
torlauncher.bootstrapStatus.ap_handshake=Building circuits: Negotiating with a Tor relay
torlauncher.bootstrapStatus.ap_handshake_done=Building circuits: Finished negotiating with a Tor relay
torlauncher.bootstrapStatus.circuit_create=Building circuits: Establishing a Tor circuit
torlauncher.bootstrapStatus.done=Connected to the Tor network!

torlauncher.bootstrapWarning.done=done
torlauncher.bootstrapWarning.connectrefused=connection refused
torlauncher.bootstrapWarning.misc=miscellaneous
torlauncher.bootstrapWarning.resourcelimit=insufficient resources
torlauncher.bootstrapWarning.identity=identity mismatch
torlauncher.bootstrapWarning.timeout=connection timeout
torlauncher.bootstrapWarning.noroute=no route to host
torlauncher.bootstrapWarning.ioerror=read/write error
torlauncher.bootstrapWarning.pt_missing=missing pluggable transport

torlauncher.nsresult.NS_ERROR_NET_RESET=The connection to the server was lost.
torlauncher.nsresult.NS_ERROR_CONNECTION_REFUSED=Could not connect to the server.
torlauncher.nsresult.NS_ERROR_PROXY_CONNECTION_REFUSED=Could not connect to the proxy.
